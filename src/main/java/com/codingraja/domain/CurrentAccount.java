package com.codingraja.domain;

public class CurrentAccount extends Account {
	private Double mab;  // Monthly Average Balance
	private Long maxTxn; // Max Transaction Limit
	
	public CurrentAccount(){}
	public CurrentAccount(Double balance, Long customerId, 
								 Double mab, Long maxTxn) {
		super(balance, customerId);
		this.mab = mab;
		this.maxTxn = maxTxn;
	}

	public Double getMab() {
		return mab;
	}

	public void setMab(Double mab) {
		this.mab = mab;
	}

	public Long getMaxTxn() {
		return maxTxn;
	}

	public void setMaxTxn(Long maxTxn) {
		this.maxTxn = maxTxn;
	}
}
