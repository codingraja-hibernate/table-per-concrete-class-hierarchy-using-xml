package com.codingraja.domain;

public class SavingAccount extends Account {
	private Double intrestRate;
	
	public SavingAccount(){}
	public SavingAccount(Double balance, Long customerId, Double intrestRate) {
		super(balance, customerId);
		this.intrestRate = intrestRate;
	}
	
	public Double getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(Double intrestRate) {
		this.intrestRate = intrestRate;
	}
}
